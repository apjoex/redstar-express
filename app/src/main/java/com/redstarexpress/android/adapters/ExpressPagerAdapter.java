package com.redstarexpress.android.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.redstarexpress.android.ui.fragments.ExpressCentresList;
import com.redstarexpress.android.ui.fragments.ExpressCentresMap;

/**
 * Created by apjoex on 31/01/2018.
 */

public class ExpressPagerAdapter extends FragmentStatePagerAdapter {

    public ExpressPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                return new ExpressCentresList();
            default:
                return new ExpressCentresMap();
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "List View";
            default:
                return "Map View";
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
