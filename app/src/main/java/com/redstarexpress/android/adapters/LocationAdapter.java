package com.redstarexpress.android.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.redstarexpress.android.R;
import com.redstarexpress.android.models.Location;
import com.redstarexpress.android.ui.activities.LocationDetail;

import java.util.ArrayList;

/**
 * Created by apjoex on 31/01/2018.
 */

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Location> locations;
    private LayoutInflater inflater;

    public LocationAdapter(Context mContext, ArrayList<Location> mlocations) {
        context = mContext;
        locations = mlocations;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.location_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Location location = locations.get(position);

        holder.centreName.setText(location.getLocation());
        holder.centreAddress.setText(location.getAddress()+" "+location.getCity()+", "+location.getState());

        holder.cardBody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LocationDetail.class);
                intent.putExtra("location", location);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return locations.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView centreName;
        TextView centreAddress;
        CardView cardBody;

        public ViewHolder(View itemView) {
            super(itemView);
            cardBody = itemView.findViewById(R.id.cardBody);
            centreName = itemView.findViewById(R.id.centreName);
            centreAddress = itemView.findViewById(R.id.centreAddress);
        }
    }

}
