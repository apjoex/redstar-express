package com.redstarexpress.android.adapters;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;

import com.redstarexpress.android.models.OnboardingItem;
import com.redstarexpress.android.ui.activities.OnBoarding;
import com.redstarexpress.android.ui.fragments.OnBoardingFragment;

import java.util.ArrayList;

/**
 * Created by apjoex on 17/01/2018.
 */

public class OnBoardingAdapter extends FragmentStatePagerAdapter {

    private AppCompatActivity mActivity;
    private ArrayList<OnboardingItem> mItems = new ArrayList<>();

    public OnBoardingAdapter(FragmentManager fm, AppCompatActivity activity, ArrayList<OnboardingItem> onboardingItems) {
        super(fm);
        mActivity = activity;
        mItems = onboardingItems;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new OnBoardingFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("item",mItems.get(position));
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }
}
