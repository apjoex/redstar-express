package com.redstarexpress.android.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.redstarexpress.android.R;
import com.redstarexpress.android.models.TrackData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by apjoex on 04/02/2018.
 */

public class TrackingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<TrackData> data;
    private LayoutInflater inflater;

    public TrackingAdapter(Context context, ArrayList<TrackData> trackDatas) {
        data = trackDatas;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(inflater.inflate(R.layout.track_history_item, parent, false));
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).getViewType();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TrackData trackData = data.get(position);
        ViewHolder viewHolder = ((ViewHolder)holder);
        viewHolder.status.setText(trackData.getStatus());
        viewHolder.date.setText(getFormattedDate(trackData.getDate()));
        viewHolder.time.setText(trackData.getScanTime());
        viewHolder.location.setText(trackData.getLocation());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView date, time, status, location;

        ViewHolder(View itemView) {
            super(itemView);
            status = itemView.findViewById(R.id.status);
            date = itemView.findViewById(R.id.date);
            time = itemView.findViewById(R.id.time);
            location = itemView.findViewById(R.id.location);
        }
    }


    private String getFormattedDate(String stringDate) {
        String pattern = "dd/MM/yyyy";
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
        SimpleDateFormat format2 = new SimpleDateFormat(pattern, Locale.getDefault());
        Date date = null;
        try {
            date = df.parse(stringDate);
            return  format2.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

}
