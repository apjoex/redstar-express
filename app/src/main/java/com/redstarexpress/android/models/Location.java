package com.redstarexpress.android.models;

import java.io.Serializable;

/**
 * Created by apjoex on 31/01/2018.
 */

public class Location implements Serializable{
    private String location, Regions, Address, City, State;
    private Double Latitude, Longitude;

    public Location(String location, String regions, String address, String city, String state, Double latitude, Double longitude) {
        this.location = location;
        Regions = regions;
        Address = address;
        City = city;
        State = state;
        Latitude = latitude;
        Longitude = longitude;
    }

    public Location() {
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRegions() {
        return Regions;
    }

    public void setRegions(String regions) {
        Regions = regions;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }
}
