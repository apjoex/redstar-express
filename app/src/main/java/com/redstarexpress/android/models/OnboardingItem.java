package com.redstarexpress.android.models;

import java.io.Serializable;

/**
 * Created by apjoex on 17/01/2018.
 */

public class OnboardingItem implements Serializable {

    private int imageResource;
    private String header, description;

    public OnboardingItem(int imageResource, String header, String description) {
        this.imageResource = imageResource;
        this.header = header;
        this.description = description;
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
