package com.redstarexpress.android.models;

/**
 * Created by apjoex on 31/01/2018.
 */

public class Station {

    private String station_code, station_description;

    public Station(String station_code, String station_description) {
        this.station_code = station_code;
        this.station_description = station_description;
    }

    public Station() {
    }

    public String getStation_code() {
        return station_code;
    }

    public void setStation_code(String station_code) {
        this.station_code = station_code;
    }

    public String getStation_description() {
        return station_description;
    }

    public void setStation_description(String station_description) {
        this.station_description = station_description;
    }
}
