package com.redstarexpress.android.models;


/**
 * Created by apjoex on 04/02/2018.
 */

public class TrackData {
    private String POD = "",
            DeliveryDate = "", DeliveryTime = "", AWBNO = "", Origin, Destination, Status, Date, ScanDate, ScanDateShort, ScanTime, Location, OriginStation, DestinationStation, sDexCodes;

    private int viewType = 0;

    public TrackData(String POD, String deliveryDate, String deliveryTime, String AWBNO, String origin, String destination, String status, String date, String scanDate, String scanDateShort, String scanTime, String location, String originStation, String destinationStation, String sDexCodes) {
        this.POD = POD;
        DeliveryDate = deliveryDate;
        DeliveryTime = deliveryTime;
        this.AWBNO = AWBNO;
        Origin = origin;
        Destination = destination;
        Status = status;
        Date = date;
        ScanDate = scanDate;
        ScanDateShort = scanDateShort;
        ScanTime = scanTime;
        Location = location;
        OriginStation = originStation;
        DestinationStation = destinationStation;
        this.sDexCodes = sDexCodes;
    }

    public TrackData() {

    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public String getPOD() {
        return POD;
    }

    public void setPOD(String POD) {
        this.POD = POD;
    }

    public String getDeliveryDate() {
        return DeliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        DeliveryDate = deliveryDate;
    }

    public String getDeliveryTime() {
        return DeliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        DeliveryTime = deliveryTime;
    }

    public String getAWBNO() {
        return AWBNO;
    }

    public void setAWBNO(String AWBNO) {
        this.AWBNO = AWBNO;
    }

    public String getOrigin() {
        return Origin;
    }

    public void setOrigin(String origin) {
        Origin = origin;
    }

    public String getDestination() {
        return Destination;
    }

    public void setDestination(String destination) {
        Destination = destination;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getScanDate() {
        return ScanDate;
    }

    public void setScanDate(String scanDate) {
        ScanDate = scanDate;
    }

    public String getScanDateShort() {
        return ScanDateShort;
    }

    public void setScanDateShort(String scanDateShort) {
        ScanDateShort = scanDateShort;
    }

    public String getScanTime() {
        return ScanTime;
    }

    public void setScanTime(String scanTime) {
        ScanTime = scanTime;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getOriginStation() {
        return OriginStation;
    }

    public void setOriginStation(String originStation) {
        OriginStation = originStation;
    }

    public String getDestinationStation() {
        return DestinationStation;
    }

    public void setDestinationStation(String destinationStation) {
        DestinationStation = destinationStation;
    }

    public String getsDexCodes() {
        return sDexCodes;
    }

    public void setsDexCodes(String sDexCodes) {
        this.sDexCodes = sDexCodes;
    }
}
