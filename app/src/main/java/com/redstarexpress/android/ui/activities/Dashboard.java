package com.redstarexpress.android.ui.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.redstarexpress.android.R;
import com.redstarexpress.android.ui.fragments.ExpressLocation;
import com.redstarexpress.android.ui.fragments.InAppBrowser;
import com.redstarexpress.android.ui.fragments.OpenAccount;
import com.redstarexpress.android.ui.fragments.PickupRequest;
import com.redstarexpress.android.ui.fragments.ProductsServices;
import com.redstarexpress.android.ui.fragments.SME1000;
import com.redstarexpress.android.ui.fragments.ShipNow;
import com.redstarexpress.android.ui.fragments.Support;
import com.redstarexpress.android.ui.fragments.TrackDomesticShipment;
import com.redstarexpress.android.ui.fragments.TrackInternationalShipment;
import com.redstarexpress.android.ui.fragments.TransitTimeEstimate;

public class Dashboard extends AppCompatActivity
{

    TextView domesticShipment,shipNow, internationalShipment, openAccount, pickupRequest, transitTime, findExpressCentre, productServices, sme, liveChat, support;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        domesticShipment = findViewById(R.id.domesticShipment);
        internationalShipment = findViewById(R.id.internationalShipment);
        openAccount = findViewById(R.id.openAccount);
        pickupRequest = findViewById(R.id.pickupRequest);
        transitTime = findViewById(R.id.transitTime);
        shipNow = findViewById(R.id.shipNow);
        findExpressCentre = findViewById(R.id.findExpressCentre);
        productServices = findViewById(R.id.productServices);
        sme = findViewById(R.id.sme);
        liveChat = findViewById(R.id.liveChat);
        support = findViewById(R.id.support);

        setFragment(new TrackDomesticShipment());

        clickEvents();
    }

    private void clickEvents() {

        domesticShipment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new TrackDomesticShipment());
            }
        });

        internationalShipment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new TrackInternationalShipment());
            }
        });


        openAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new OpenAccount());
            }
        });

        pickupRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new PickupRequest());
            }
        });

        transitTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new TransitTimeEstimate());
            }
        });

        findExpressCentre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new ExpressLocation());
            }
        });

        shipNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new ShipNow());
            }
        });

        productServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new ProductsServices());
            }
        });

        sme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setFragment(new SME1000());
            }
        });

        liveChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new InAppBrowser();
                Bundle bundle = new Bundle();
                bundle.putString("url","http://v2.zopim.com/widget/livechat.html?key=5D81Lm1WEViR33GVvw4vIjmTtQLIsqco");
                bundle.putString("title","Live Chat");
                fragment.setArguments(bundle);
                setFragment(fragment);
            }
        });

        support.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFragment(new Support());
            }
        });
    }

    public void setFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame, fragment)
                .disallowAddToBackStack()
                .commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    public void openDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.openDrawer(GravityCompat.START);
    }
}
