package com.redstarexpress.android.ui.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.redstarexpress.android.R;
import com.redstarexpress.android.models.Location;

public class LocationDetail extends AppCompatActivity {

    Context context;
    Location location;
    ImageView mapImg;
    TextView name, city, address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_detail);

        context = this;
        mapImg = findViewById(R.id.mapImg);
        name = findViewById(R.id.name);
        city = findViewById(R.id.city);
        address = findViewById(R.id.address);

        location = (Location) getIntent().getSerializableExtra("location");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(location.getLocation());

        String static_map = "http://maps.google.com/maps/api/staticmap?zoom=14&size=600x400&maptype=roadmap%20&markers=color:green|label:P|"+location.getLatitude()+","+location.getLongitude()+"&sensor=false";

        //Toast.makeText(context, static_map, Toast.LENGTH_SHORT).show();
        //Log.d("LOGGER", static_map);

        Glide.with(context)
                .load(static_map)
                .error(R.mipmap.ic_launcher)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(mapImg);


        name.setText(location.getLocation());
        address.setText(location.getAddress());
        city.setText(location.getCity()+" "+location.getState());


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
