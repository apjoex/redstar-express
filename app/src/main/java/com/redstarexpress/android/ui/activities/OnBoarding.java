package com.redstarexpress.android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.redstarexpress.android.R;
import com.redstarexpress.android.adapters.OnBoardingAdapter;
import com.redstarexpress.android.models.OnboardingItem;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

public class OnBoarding extends AppCompatActivity {

    OnBoardingAdapter adapter;
    ViewPager pager;
    CircleIndicator indicator;
    ArrayList<OnboardingItem> onboardingItems = new ArrayList<>();
    Button loginBtn, signUpBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_on_boarding);

        getSupportActionBar().hide();

        pager = findViewById(R.id.pager);
        loginBtn = findViewById(R.id.logInBtn);
        signUpBtn = findViewById(R.id.signUpBtn);
        indicator = findViewById(R.id.indicator);

        onboardingItems.add(new OnboardingItem(R.mipmap.page_one, "Track Shipments", "Easily track and check the status of your domestic shipments."));
        onboardingItems.add(new OnboardingItem(R.mipmap.page_two, "Locate RSE Centres","Use our dynamic Express centre locator to find the nearest RSE Centre close to you!"));
        onboardingItems.add(new OnboardingItem(R.mipmap.page_three, "Request Pick-ups", "Schedule, view, edit and request pickups with just a few taps."));

        adapter = new OnBoardingAdapter(getSupportFragmentManager(),OnBoarding.this, onboardingItems);
        pager.setAdapter(adapter);
        indicator.setViewPager(pager);

//        String url = "https://web.redstarplc.com:8444/TrackingService/tracking.asmx/Station_Fetch";
//        Fuel.post(url).responseString(new Handler<String>() {
//            @Override
//            public void success(Request request, Response response, String s) {
//                Toast.makeText(OnBoarding.this, s, Toast.LENGTH_SHORT).show();
//
//                s = s.replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>","");
//                s = s.replace("<string xmlns=\"http://tempuri.org/\">","");
//                s = s.replace("</string>","");
//                Toast.makeText(OnBoarding.this, s, Toast.LENGTH_SHORT).show();
//
//
//             //  XmlToJson xmlToJson = new XmlToJson.Builder(s).build();
//                try {
//                    JSONObject baseObject = new JSONObject(s);
//                    JSONArray table = baseObject.getJSONArray("Table");
//                    Log.d("RESPONSE_NEW", s);
//                  //  Toast.makeText(OnBoarding.this, "Table size: "+table.length(), Toast.LENGTH_SHORT).show();
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//
//
//
//            }
//
//            @Override
//            public void failure(Request request, Response response, FuelError fuelError) {
//                Toast.makeText(OnBoarding.this, "Error: "+fuelError, Toast.LENGTH_SHORT).show();
//            }
//        });

        clickEvents();
    }

    private void clickEvents() {
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(OnBoarding.this, Dashboard.class));
                finish();
            }
        });
    }

}
