package com.redstarexpress.android.ui.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.redstarexpress.android.R;
import com.redstarexpress.android.adapters.TrackingAdapter;
import com.redstarexpress.android.models.TrackData;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TrackingDetails extends AppCompatActivity {

    private ArrayList<TrackData> trackDataHistory = new ArrayList<>();
    private Context context;

    private RecyclerView trackHistory;
    private TextView textView;
    private LinearLayout detailCardView;

    TextView waybill_number, origin_station, destination_station;
    private String number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;

        setContentView(R.layout.activity_tracking_details);
        String trackDatas = getIntent().getExtras().getString("data", "");

        Gson gson = new Gson();
        trackDataHistory = gson.fromJson(trackDatas, new TypeToken<ArrayList<TrackData>>(){}.getType());

        number = getIntent().getStringExtra("number");

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("#" + number);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        trackHistory = findViewById(R.id.trackHistory);
        textView = findViewById(R.id.delivery_details);
        detailCardView = findViewById(R.id.detail_card_view);

//
        waybill_number = findViewById(R.id.waybill_number);
        origin_station = findViewById(R.id.origin_station);
        destination_station = findViewById(R.id.destination_station);

        showTrackDeliveryDetails();
    }

    private void showTrackDeliveryDetails() {
        TrackData trackData = getTrackData();

        String title = getPODTitle(trackData);
        if (!TextUtils.isEmpty(title)) {
            detailCardView.setVisibility(View.VISIBLE);

            waybill_number.setText(number);
            origin_station.setText(trackData.getOriginStation());
            destination_station.setText(trackData.getDestinationStation());

            textView.setText(title);
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        trackHistory.setLayoutManager(layoutManager);
        TrackingAdapter adapter = new TrackingAdapter(context, trackDataHistory);
        trackHistory.setAdapter(adapter);
    }

    private TrackData getTrackData(){
        TrackData value = new TrackData();
        for (TrackData trackData: trackDataHistory) {
            if (!TextUtils.isEmpty(trackData.getPOD()) && !TextUtils.isEmpty(trackData.getDeliveryDate())) {
                value.setPOD(trackData.getPOD());
                value.setDestinationStation(trackData.getDestinationStation());
                value.setOriginStation(trackData.getOriginStation());
                value.setDeliveryDate(trackData.getDeliveryDate());
                break;
            }
        }
        return value;
    }

    private String getPODTitle(TrackData trackData){
        String value = "";
        if (trackData != null) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
            SimpleDateFormat format2 = new SimpleDateFormat("EEE, d MMM yyyy", Locale.getDefault());
            SimpleDateFormat format3 = new SimpleDateFormat(" HH:mm a", Locale.getDefault());
            try {
                Date date = df.parse(trackData.getDeliveryDate());
                String niceDate = format2.format(date);
                String niceTime = format3.format(date);
                value = "This shipment was delivered to " +trackData.getPOD().trim()+ " on "+niceDate+" at " + niceTime;
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return value;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
