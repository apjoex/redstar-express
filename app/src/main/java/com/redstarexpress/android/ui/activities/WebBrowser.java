package com.redstarexpress.android.ui.activities;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.redstarexpress.android.R;
import com.redstarexpress.android.ui.fragments.InAppBrowser;

public class WebBrowser extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_browser);


        String title = getIntent().getExtras().getString("title");
        String url = getIntent().getExtras().getString("url");

        Fragment fragment = new InAppBrowser();
        Bundle bundle = new Bundle();
        bundle.putString("url",url);
        bundle.putBoolean("hide",true);
        bundle.putString("title",title);
        fragment.setArguments(bundle);


        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame, fragment)
                .disallowAddToBackStack()
                .commit();


        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
