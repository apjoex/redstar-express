package com.redstarexpress.android.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.redstarexpress.android.R;
import com.redstarexpress.android.adapters.LocationAdapter;
import com.redstarexpress.android.models.Location;
import com.redstarexpress.android.utils.Store;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by apjoex on 31/01/2018.
 */

public class ExpressCentresList extends Fragment {

    Context mContext;
    String locationString;
    ArrayList<Location> locations = new ArrayList<>();
    ArrayList<Location> filteredLocations = new ArrayList<>();
    LocationAdapter locationAdapter;
    RecyclerView centresList;
    EditText searchText;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.express_centre_list, container, false);

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        centresList = view.findViewById(R.id.centresList);
        searchText = view.findViewById(R.id.searchText);

        showAllLocations();

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if(editable.toString().trim().length() > 0){
                    filteredLocations.clear();

                    for (Location location : locations){
                        if(location.getLocation().toLowerCase().contains(editable.toString().toLowerCase())
                                || location.getAddress().toLowerCase().contains(editable.toString().toLowerCase())
                                || location.getState().toLowerCase().contains(editable.toString().toLowerCase())
                                ){
                            filteredLocations.add(location);
                        }
                    }

                    locationAdapter = new LocationAdapter(mContext, filteredLocations);
                    centresList.setAdapter(locationAdapter);
                }else{
                    showAllLocations();
                }



            }
        });
    }

    private void showAllLocations() {
        locationString = Store.locations;

        try {
            JSONArray baseArray = new JSONArray(locationString);

            Type type = new TypeToken<ArrayList<Location>>(){}.getType();
            Gson gson = new Gson();

            locations = gson.fromJson(locationString, type);


            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
            centresList.setLayoutManager(layoutManager);

            locationAdapter = new LocationAdapter(mContext, locations);
            centresList.setAdapter(locationAdapter);


        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(mContext, "Error parsing JSON", Toast.LENGTH_SHORT).show();
        }
    }

}
