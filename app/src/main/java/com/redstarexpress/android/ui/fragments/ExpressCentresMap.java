package com.redstarexpress.android.ui.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.kittinunf.fuel.Fuel;
import com.github.kittinunf.fuel.core.FuelError;
import com.github.kittinunf.fuel.core.Handler;
import com.github.kittinunf.fuel.core.Request;
import com.github.kittinunf.fuel.core.Response;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.redstarexpress.android.R;
import com.redstarexpress.android.adapters.LocationAdapter;
import com.redstarexpress.android.ui.activities.LocationDetail;
import com.redstarexpress.android.utils.Store;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by apjoex on 31/01/2018.
 */

public class ExpressCentresMap extends Fragment {

    Context mContext;
    private GoogleMap mMap;
    String locationString;
    ArrayList<com.redstarexpress.android.models.Location> locations = new ArrayList<>();
    private SupportMapFragment mapFragment;
    LatLngBounds.Builder bounds = new LatLngBounds.Builder();


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.express_centre_map, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bounds = new LatLngBounds.Builder();

        locationString = Store.locations;

        try {
            JSONArray baseArray = new JSONArray(locationString);
            Type type = new TypeToken<ArrayList<com.redstarexpress.android.models.Location>>(){}.getType();
            Gson gson = new Gson();
            locations = gson.fromJson(locationString, type);
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(mContext, "Error parsing JSON", Toast.LENGTH_SHORT).show();
        }

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);


        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap googleMap) {

                mMap = googleMap;

                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(mContext, "Request for permission", Toast.LENGTH_SHORT).show();
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},1);

                    return;
                }
                googleMap.setMyLocationEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);

                //LatLng userL = googleMap.getCameraPosition().target;
                //googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userL, 13));


                for(com.redstarexpress.android.models.Location location : locations){
                    if(location.getLatitude() != null && location.getLongitude() != null){
                        LatLng coords = new LatLng(location.getLatitude(), location.getLongitude());
                        bounds.include(coords);

                        mMap.addMarker(new MarkerOptions().position(coords).title(location.getLocation()).snippet(location.getAddress()+" "+location.getCity()+", "+location.getState()));
                    }
                }
//                // Add a marker in Sydney and move the camera
//                LatLng sydney = new LatLng(-34, 151);
//                mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));
//
//
//                // Add a marker in Sydney and move the camera
                LatLng lagos = new LatLng(6.5244, 3.3792);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lagos,11));
                //map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 13));


                //mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 200));


                mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {

                        com.redstarexpress.android.models.Location mLocation = new com.redstarexpress.android.models.Location();

                        for(com.redstarexpress.android.models.Location location : locations){
                            if(location.getLatitude().equals(marker.getPosition().latitude) && location.getLongitude().equals(marker.getPosition().longitude)){
                                mLocation = location;
                            }
                        }

                        Intent intent = new Intent(mContext, LocationDetail.class);
                        intent.putExtra("location", mLocation);
                        mContext.startActivity(intent);
                    }
                });

            }

        });


    }


}
