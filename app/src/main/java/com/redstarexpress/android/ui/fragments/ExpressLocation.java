package com.redstarexpress.android.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.redstarexpress.android.R;
import com.redstarexpress.android.adapters.ExpressPagerAdapter;
import com.redstarexpress.android.ui.activities.Dashboard;

/**
 * Created by apjoex on 30/01/2018.
 */

public class ExpressLocation extends Fragment {

    ImageView menuAnchor;
    Context mContext;
    ViewPager pager;
    ExpressPagerAdapter pagerAdapter;
    TabLayout tabs;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.express_location, container, false);

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        menuAnchor = view.findViewById(R.id.menuAnchor);

        pager = view.findViewById(R.id.pager);
        pagerAdapter = new ExpressPagerAdapter(getChildFragmentManager());
        pager.setAdapter(pagerAdapter);

        tabs = view.findViewById(R.id.tabs);
        tabs.setupWithViewPager(pager);

        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        clickEvents();
    }

    private void clickEvents() {
        menuAnchor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Dashboard) mContext).openDrawer();
            }
        });
    }

}
