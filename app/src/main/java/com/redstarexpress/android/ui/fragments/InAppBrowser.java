package com.redstarexpress.android.ui.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.redstarexpress.android.R;
import com.redstarexpress.android.ui.activities.Dashboard;

/**
 * Created by apjoex on 06/02/2018.
 */

public class InAppBrowser extends Fragment {

    ImageView menuAnchor;
    TextView toolbarTitle;
    Context mContext;
    WebView webView;
    String url, title;
    ImageButton backButton, forwardButton;
    boolean hide = false;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        url = getArguments().getString("url");
        title = getArguments().getString("title");
        hide = getArguments().getBoolean("hide");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.browser, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        menuAnchor = view.findViewById(R.id.menuAnchor);
        toolbarTitle = view.findViewById(R.id.title);
        webView = view.findViewById(R.id.webView);
        backButton = view.findViewById(R.id.backButton);
        ConstraintLayout constraint = view.findViewById(R.id.constraint);
        CardView cardView = view.findViewById(R.id.cardView);
        forwardButton = view.findViewById(R.id.forwardButton);


        if(hide){
            ViewGroup.MarginLayoutParams layoutParams =
                    (ViewGroup.MarginLayoutParams) cardView.getLayoutParams();
            layoutParams.setMargins(0, 0, 0, 0);
            cardView.requestLayout();
            menuAnchor.setVisibility(View.GONE);
            constraint.setVisibility(View.GONE);
        }

        toolbarTitle.setText(title);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);

        webView.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(mContext, description, Toast.LENGTH_SHORT).show();
            }
            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });

        clickEvents();

        webView .loadUrl(url);
    }



    private void clickEvents() {
        menuAnchor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Dashboard) mContext).openDrawer();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // Going back if canGoBack true
                if(webView.canGoBack()){
                    webView.goBack();
                }
            }
        });

        forwardButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Go Forward if canGoForward is frue

                if(webView.canGoForward()){
                    webView.goForward();
                }
            }
        });
    }

}
