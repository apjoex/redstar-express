package com.redstarexpress.android.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.redstarexpress.android.R;
import com.redstarexpress.android.models.OnboardingItem;

/**
 * Created by apjoex on 17/01/2018.
 */

public class OnBoardingFragment extends Fragment {

    OnboardingItem onboardingItem;
    TextView header, description;
    ImageView img;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onboardingItem = (OnboardingItem) getArguments().getSerializable("item");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View customView = inflater.inflate(R.layout.fragment_onboarding, container, false);
        return customView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        header = view.findViewById(R.id.header);
        description = view.findViewById(R.id.description);
        img = view.findViewById(R.id.img);
        updateUI();
    }

    private void updateUI() {
        img.setImageResource(onboardingItem.getImageResource());
        header.setText(onboardingItem.getHeader());
        description.setText(onboardingItem.getDescription());
    }
}

