package com.redstarexpress.android.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.creativityapps.gmailbackgroundlibrary.BackgroundMail;
import com.redstarexpress.android.R;
import com.redstarexpress.android.ui.activities.Dashboard;

/**
 * Created by apjoex on 30/01/2018.
 */

public class OpenAccount extends Fragment {

    ImageView menuAnchor;
    Context mContext;
    Button submitButton;
    TextInputLayout fullName, email, companyName, phoneNumber, mobileNumber, city, address;
    Spinner accountType, shipFrequency;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.open_account, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        menuAnchor = view.findViewById(R.id.menuAnchor);

        submitButton = view.findViewById(R.id.submitButton);
        fullName = view.findViewById(R.id.fullName);
        email = view.findViewById(R.id.email);
        companyName = view.findViewById(R.id.companyName);
        phoneNumber = view.findViewById(R.id.phoneNumber);
        mobileNumber = view.findViewById(R.id.mobileNumber);
        accountType = view.findViewById(R.id.accountType);
        shipFrequency = view.findViewById(R.id.shipFrequency);
        city = view.findViewById(R.id.city);
        address = view.findViewById(R.id.address);

        clickEvents();
    }

    private void clickEvents() {
        menuAnchor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Dashboard) mContext).openDrawer();
            }
        });


        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(validateFields()){
                    BackgroundMail.newBuilder(mContext)
                            .withUsername("redstarapp01@gmail.com")
                            .withPassword("RED_STAR")
                            .withMailto("cs@redstarplc.com, okikelomo@redstarplc.com, iabdullahi@redstarplc.com, gakande@redstarplc.com")
                            .withType(BackgroundMail.TYPE_PLAIN)
                            .withSubject("OPEN ACCOUNT REQUEST")
                            .withBody("Full name: "+fullName.getEditText().getText().toString()+"\n\nEmail: "+email.getEditText().getText().toString()+"\n\n"+"Company name: "+companyName.getEditText().getText().toString()
                                    +"\n\nPhone number: "+phoneNumber.getEditText().getText().toString()+"\n\nMobile number: "+mobileNumber.getEditText().getText().toString()+"\n\nAccount Type: "+accountType.getSelectedItem().toString()
                                    +"\n\nShipping Frequency: "+shipFrequency.getSelectedItem().toString()+"\n\nCity/Location: "+city.getEditText().getText().toString()+"\n\nAddress: "+address.getEditText().getText().toString())
                            .withOnSuccessCallback(new BackgroundMail.OnSuccessCallback() {
                                @Override
                                public void onSuccess() {
                                    //do some magic
                                    Log.e("email","Sent::");
                                     Toast.makeText(mContext, "Open Account request sent successfully.", Toast.LENGTH_SHORT).show();

                                    fullName.getEditText().getText().clear();
                                    email.getEditText().getText().clear();
                                    companyName.getEditText().getText().clear();
                                    phoneNumber.getEditText().getText().clear();
                                    mobileNumber.getEditText().getText().clear();
                                    accountType.setSelection(0);
                                    shipFrequency.setSelection(0);
                                    city.getEditText().getText().clear();
                                    address.getEditText().getText().clear();


                                }
                            })
                            .withOnFailCallback(new BackgroundMail.OnFailCallback() {
                                @Override
                                public void onFail() {
                                      Toast.makeText(mContext, "Unable to send. Please try again", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .send();
                }
            }
        });
    }

    private boolean validateFields() {

        if(fullName.getEditText().getText().toString().isEmpty()){
            fullName.setError("Enter full name");
            return false;
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email.getEditText().getText().toString()).matches()){
            email.setError("Enter a valid email address");
            return false;
        }

        if(mobileNumber.getEditText().getText().toString().isEmpty()){
            mobileNumber.setError("Enter mobile number");
            return false;
        }

        return true;
    }
}
