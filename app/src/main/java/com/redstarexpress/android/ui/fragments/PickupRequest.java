package com.redstarexpress.android.ui.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.creativityapps.gmailbackgroundlibrary.BackgroundMail;
import com.redstarexpress.android.R;
import com.redstarexpress.android.ui.activities.Dashboard;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by apjoex on 30/01/2018.
 */

public class PickupRequest extends Fragment {

    ImageView menuAnchor;
    Context mContext;
    Button submitButton;
    private TextInputLayout fullName, email, companyName, mobileNumber, packageNumber, weight, city, state, address, description;
    private TextInputLayout startTime, pickupDate;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pickup_request, container, false);

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        menuAnchor = view.findViewById(R.id.menuAnchor);

        fullName = view.findViewById(R.id.fullName);
        email = view.findViewById(R.id.email);
        companyName = view.findViewById(R.id.companyName);
        mobileNumber = view.findViewById(R.id.mobileNumber);
        packageNumber = view.findViewById(R.id.packageNumber);
        weight = view.findViewById(R.id.weight);
        city = view.findViewById(R.id.city);
        state = view.findViewById(R.id.state);
        address = view.findViewById(R.id.address);
        description = view.findViewById(R.id.description);
        startTime = view.findViewById(R.id.readyTime);
        pickupDate = view.findViewById(R.id.pickupDate);
        submitButton = view.findViewById(R.id.submitButton);

        clickEvents();
    }

    private void clickEvents() {
        menuAnchor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Dashboard) mContext).openDrawer();
            }
        });

        startTime.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b){
                    // TODO Auto-generated method stub
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            startTime.getEditText().setText( selectedHour + ":" + selectedMinute);
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.setTitle("Set Ready Time");
                    mTimePicker.show();

                }
            }
        });

        pickupDate.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {

                if(b){
                    final Calendar mcurrentTime = Calendar.getInstance();
                    int year = mcurrentTime.get(Calendar.YEAR);
                    int month = mcurrentTime.get(Calendar.MONTH);
                    int day = mcurrentTime.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog mDatePicker;
                    mDatePicker = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)     {

                            String myFormat = "MMM dd, yyyy"; //In which you need put here
                            SimpleDateFormat sdformat = new SimpleDateFormat(myFormat, Locale.US);
                            mcurrentTime.set(Calendar.YEAR, year);
                            mcurrentTime.set(Calendar.MONTH, monthOfYear);
                            mcurrentTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                            pickupDate.getEditText().setText(sdformat.format(mcurrentTime.getTime()));

                        }
                    },year, month, day);
                    mDatePicker.setTitle("Set Pickup Date");
                    mDatePicker.show();
                }

            }
        });


        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(validateFields()){
                    BackgroundMail.newBuilder(mContext)
                            .withUsername("redstarapp01@gmail.com")
                            .withPassword("RED_STAR")
                            //.withMailto("apjoex@gmail.com")
                            .withType(BackgroundMail.TYPE_PLAIN)
                            .withSubject("Pickup Request")
                            .withBody("Full name: "+fullName.getEditText().getText().toString()+"\n\nEmail: "+email.getEditText().getText().toString()+"\n\n"+"Company name: "+companyName.getEditText().getText().toString()
                                    +"\n\nMobile number: "+mobileNumber.getEditText().getText().toString()+"\n\nNo. of packages: "+packageNumber.getEditText().getText().toString()
                                    +"\n\nWeight (KG): "+weight.getEditText().getText().toString()+"\n\nPickup date: "+pickupDate.getEditText().getText().toString()+"\n\nReady Time: "+startTime.getEditText().getText().toString()
                                    +"\n\nState: "+state.getEditText().getText().toString() + "\n\nCity: "+city.getEditText().getText().toString()+"\n\nPickup Address: "+address.getEditText().getText().toString()
                                    +"\n\nDescription of shipment: "+description.getEditText().getText().toString())
                            .withOnSuccessCallback(new BackgroundMail.OnSuccessCallback() {
                                @Override
                                public void onSuccess() {
                                    //do some magic
                                    Log.e("email","Sent::");
                                    Toast.makeText(mContext, "Pickup request sent successfully", Toast.LENGTH_SHORT).show();
                                    fullName.getEditText().getText().clear();
                                    email.getEditText().getText().clear();
                                    companyName.getEditText().getText().clear();
                                    mobileNumber.getEditText().getText().clear();
                                    packageNumber.getEditText().getText().clear();
                                    weight.getEditText().getText().clear();
                                    pickupDate.getEditText().getText().clear();
                                    startTime.getEditText().getText().clear();
                                    state.getEditText().getText().clear();
                                    city.getEditText().getText().clear();
                                    address.getEditText().getText().clear();
                                    description.getEditText().getText().clear();
                                }
                            })
                            .withOnFailCallback(new BackgroundMail.OnFailCallback() {
                                @Override
                                public void onFail() {
                                    Toast.makeText(mContext, "Unable to send request. Please try again", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .send();
                }


            }
        });



    }

    private boolean validateFields() {

        if(fullName.getEditText().getText().toString().isEmpty()){
            fullName.setError("Enter full name");
            return false;
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email.getEditText().getText().toString()).matches()){
            email.setError("Enter a valid email address");
            return false;
        }

        if(mobileNumber.getEditText().getText().toString().isEmpty()){
            mobileNumber.setError("Enter mobile number");
            return false;
        }

        return true;
    }
}
