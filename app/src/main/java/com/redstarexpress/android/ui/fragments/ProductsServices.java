package com.redstarexpress.android.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.redstarexpress.android.R;
import com.redstarexpress.android.ui.activities.Dashboard;

/**
 * Created by apjoex on 31/01/2018.
 */

public class ProductsServices extends Fragment {

    ImageView menuAnchor;
    Context mContext;
    RelativeLayout expressBox, logisticsBox, frieghtBox, supportBox;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.products_services, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        menuAnchor = view.findViewById(R.id.menuAnchor);
        expressBox = view.findViewById(R.id.expressBox);
        logisticsBox = view.findViewById(R.id.logisticsBox);
        frieghtBox = view.findViewById(R.id.frieghtBox);
        supportBox = view.findViewById(R.id.supportBox);

        clickEvents();
    }

    private void clickEvents() {
        menuAnchor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Dashboard) mContext).openDrawer();
            }
        });

        expressBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Fragment fragment = new InAppBrowser();
                Bundle bundle = new Bundle();
                bundle.putString("url","http://rse.redstarplc.com/services/");
                bundle.putString("title","Express");
                fragment.setArguments(bundle);
                ((Dashboard)mContext).setFragment(fragment);
            }
        });


        logisticsBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new InAppBrowser();
                Bundle bundle = new Bundle();
                bundle.putString("url","http://rsl.redstarplc.com/logistics-solutions/");
                bundle.putString("title","Logistics");
                fragment.setArguments(bundle);
                ((Dashboard)mContext).setFragment(fragment);
            }
        });

        frieghtBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new InAppBrowser();
                Bundle bundle = new Bundle();
                bundle.putString("url","http://rsf.redstarplc.com/about-us/");
                bundle.putString("title","Freight");
                fragment.setArguments(bundle);
                ((Dashboard)mContext).setFragment(fragment);
            }
        });

        supportBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new InAppBrowser();
                Bundle bundle = new Bundle();
                bundle.putString("url","http://rss.redstarplc.com/products-and-services/");
                bundle.putString("title","Support services");
                fragment.setArguments(bundle);
                ((Dashboard)mContext).setFragment(fragment);
            }
        });
    }

}
