package com.redstarexpress.android.ui.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.kittinunf.fuel.Fuel;
import com.github.kittinunf.fuel.core.FuelError;
import com.github.kittinunf.fuel.core.Handler;
import com.github.kittinunf.fuel.core.Request;
import com.github.kittinunf.fuel.core.Response;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.redstarexpress.android.R;
import com.redstarexpress.android.models.TrackData;
import com.redstarexpress.android.ui.activities.Dashboard;
import com.redstarexpress.android.ui.activities.TrackingDetails;
import com.redstarexpress.android.ui.activities.WebBrowser;
import com.redstarexpress.android.utils.Functions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import kotlin.Pair;

/**
 * Created by apjoex on 29/01/2018.
 */

public class TrackInternationalShipment extends Fragment {

    ImageView menuAnchor;
    Context mContext;
    Button trackBtn;
    EditText trackingNumber;
    ArrayList<TrackData> trackDatas = new ArrayList<>();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.track_international_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        menuAnchor = view.findViewById(R.id.menuAnchor);
        trackingNumber = view.findViewById(R.id.trackingNumber);
        trackBtn = view.findViewById(R.id.trackBtn);


        view.findViewById(R.id.parent_content).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Functions.hideSoftKeyboard(getContext());
                return false;
            }
        });

        clickEvents();
    }

    private void clickEvents() {
        menuAnchor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Dashboard) mContext).openDrawer();
            }
        });

        trackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateFields()){
                    trackShipment(trackingNumber.getText().toString());
                }
            }
        });
    }

    private void trackShipment(final String trackingNo) {

        Intent intent = new Intent(getActivity(), WebBrowser.class);
        intent.putExtra("title","Tracking " +trackingNo);
        intent.putExtra("hide",true);
        intent.putExtra("url","https://www.fedex.com/apps/fedextrack/?tracknumber=" + trackingNo + "&cntry_code=ng");
        startActivity(intent);

//        Fragment fragment = new InAppBrowser();
//        Bundle bundle = new Bundle();
//        bundle.putString("title","Tracking " +trackingNo);
//        bundle.putString("url","https://www.fedex.com/apps/fedextrack/?tracknumber=" + trackingNo + "&cntry_code=ng");
//        fragment.setArguments(bundle);
//
//
//        getActivity().getSupportFragmentManager().beginTransaction()
//                .replace(R.id.frame, fragment)
//                .disallowAddToBackStack()
//                .commit();


//        final ProgressDialog progressDialog = ProgressDialog.show(mContext, null, "Please wait...", false, false);
//
//        String url = "https://web.redstarplc.com:8444/TrackingService/Tracking.asmx/Tracking_Fetch";
//        List< Pair<String, Object>> params = new ArrayList<>();
//        params.add(new Pair<String, Object>("Awbno", trackingNo));
//
//        Fuel.post(url, params).responseString(new Handler<String>() {
//
//            @Override
//            public void success(Request request, Response response, String s) {
//
//                trackDatas.clear();
//
//                progressDialog.dismiss();
//
//                s = s.replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>","");
//                s = s.replace("<string xmlns=\"http://tempuri.org/\">","");
//                s = s.replace("</string>","");
//
//                try {
//                    JSONObject baseObject = new JSONObject(s);
//                    JSONArray table = baseObject.getJSONArray("Table");
//
//                    Type type = new TypeToken<ArrayList<TrackData>>(){}.getType();
//                    Gson gson = new Gson();
//                    trackDatas = gson.fromJson(table.toString(), type);
//
//                    Intent intent = new Intent(mContext, TrackingDetails.class);
//                    intent.putExtra("data", trackDatas);
//                    intent.putExtra("number", trackingNo);
//                    startActivity(intent);
//
//                    //  Toast.makeText(mContext, "Tracking info is "+trackDatas.size(), Toast.LENGTH_SHORT).show();
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Toast.makeText(mContext, s, Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void failure(Request request, Response response, FuelError fuelError) {
//                progressDialog.dismiss();
//                Toast.makeText(mContext, "Error: "+fuelError, Toast.LENGTH_SHORT).show();
//            }
//        });

    }

    private boolean validateFields() {

        if(trackingNumber.getText().toString().trim().isEmpty()){
            trackingNumber.setError("Please enter tracking number");
            return false;
        }

        return true;
    }
}
