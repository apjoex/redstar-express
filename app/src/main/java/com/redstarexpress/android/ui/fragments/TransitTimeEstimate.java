package com.redstarexpress.android.ui.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.kittinunf.fuel.Fuel;
import com.github.kittinunf.fuel.core.FuelError;
import com.github.kittinunf.fuel.core.Handler;
import com.github.kittinunf.fuel.core.Request;
import com.github.kittinunf.fuel.core.Response;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.redstarexpress.android.R;
import com.redstarexpress.android.models.Station;
import com.redstarexpress.android.ui.activities.Dashboard;
import com.redstarexpress.android.ui.activities.OnBoarding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import kotlin.Pair;

/**
 * Created by apjoex on 30/01/2018.
 */

public class TransitTimeEstimate extends Fragment {

    ImageView menuAnchor;
    Context mContext;
    TextView fromText, toText;
    ProgressDialog progressDialog;
    ArrayList<Station> stations = new ArrayList<>();
    Station fromStation, toStation;
    Button submitButton;
    LinearLayout stageOne, stageTwo, stageThree;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.transit_time_estimate, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        menuAnchor = view.findViewById(R.id.menuAnchor);
        fromText = view.findViewById(R.id.fromText);
        toText = view.findViewById(R.id.toText);
        submitButton = view.findViewById(R.id.submitButton);

        stageOne = view.findViewById(R.id.stageOne);
        stageTwo = view.findViewById(R.id.stageTwo);
        stageThree = view.findViewById(R.id.stageThree);

        clickEvents();
    }

    private void clickEvents() {
        menuAnchor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Dashboard) mContext).openDrawer();
            }
        });

        fromText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog = ProgressDialog.show(mContext, null, "Please wait...", false, false);

                String url = "https://web.redstarplc.com:8444/TrackingService/tracking.asmx/Station_Fetch";

                Fuel.post(url).responseString(new Handler<String>() {

                    @Override
                    public void success(Request request, Response response, String s) {

                        progressDialog.dismiss();

                        s = s.replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>","");
                        s = s.replace("<string xmlns=\"http://tempuri.org/\">","");
                        s = s.replace("</string>","");

                        try {
                            JSONObject baseObject = new JSONObject(s);
                            JSONArray table = baseObject.getJSONArray("Table");

                            Type type = new TypeToken<ArrayList<Station>>(){}.getType();
                            Gson gson = new Gson();
                            stations = gson.fromJson(table.toString(), type);

                            showStations("from");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void failure(Request request, Response response, FuelError fuelError) {
                        progressDialog.dismiss();
                        Toast.makeText(mContext, "Error: "+fuelError, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        toText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog = ProgressDialog.show(mContext, null, "Please wait...", false, false);

                String url = "https://web.redstarplc.com:8444/TrackingService/tracking.asmx/Station_Fetch";

                Fuel.post(url).responseString(new Handler<String>() {

                    @Override
                    public void success(Request request, Response response, String s) {

                        progressDialog.dismiss();

                        s = s.replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>","");
                        s = s.replace("<string xmlns=\"http://tempuri.org/\">","");
                        s = s.replace("</string>","");

                        try {
                            JSONObject baseObject = new JSONObject(s);
                            JSONArray table = baseObject.getJSONArray("Table");

                            Type type = new TypeToken<ArrayList<Station>>(){}.getType();
                            Gson gson = new Gson();
                            stations = gson.fromJson(table.toString(), type);

                            showStations("to");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void failure(Request request, Response response, FuelError fuelError) {
                        progressDialog.dismiss();
                        Toast.makeText(mContext, "Error: "+fuelError, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });


        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(fromStation != null && toStation != null){

                    progressDialog = ProgressDialog.show(mContext, null, "Please wait...", false, false);
                    String url = "https://web.redstarplc.com:8444/TrackingService/tracking.asmx/TransitTime_Fetch";
                    List< Pair<String, Object>> params = new ArrayList<>();
                    params.add(new Pair<String, Object>("Origin", fromStation.getStation_code()));
                    params.add(new Pair<String, Object>("Destination", toStation.getStation_code()));

                    Fuel.post(url,params).responseString(new Handler<String>() {
                        @Override
                        public void success(Request request, Response response, String s) {
                            progressDialog.dismiss();
                           // Toast.makeText(mContext, "Response: "+s, Toast.LENGTH_SHORT).show();

                            s = s.replace("<?xml version=\"1.0\" encoding=\"utf-8\"?>","");
                            s = s.replace("<string xmlns=\"http://tempuri.org/\">","");
                            s = s.replace("</string>","");

                            try {
                                JSONObject baseobject = new JSONObject(s);
                                JSONArray table = baseobject.getJSONArray("Table");

                                JSONObject firstResult = table.getJSONObject(0);

                                AlertDialog dialog = new AlertDialog.Builder(mContext)
                                        .setTitle("Transit Time")
                                        .setMessage("Express Delivery Days: "+firstResult.optString("ExpressDeliveryDays")+"\n\nLogistics Delivery Days: "+firstResult.optString("LogisticsDeliveryDays"))
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                stageOne.setVisibility(View.VISIBLE);
                                                stageTwo.setVisibility(View.GONE);
                                                stageThree.setVisibility(View.GONE);
                                            }
                                        })
                                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        })
                                        .create();

                                dialog.show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void failure(Request request, Response response, FuelError fuelError) {
                            progressDialog.dismiss();
                            Toast.makeText(mContext, "Error: "+fuelError.toString(), Toast.LENGTH_SHORT).show();
                        }
                    });


                }else{
                    Toast.makeText(mContext, "Please choose a origin and destination station", Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

    private void showStations(final String s) {
        CharSequence[] stationNames = new CharSequence[stations.size()];
        for(int i = 0; i < stationNames.length; i++){
            stationNames[i] = stations.get(i).getStation_description();
        }

        AlertDialog dialog = new AlertDialog.Builder(mContext)
                .setTitle("Select Station")
                .setItems(stationNames, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        if(s == "from"){
                            fromStation = stations.get(i);
                            fromText.setText(fromStation.getStation_description());

                            stageTwo.setVisibility(View.VISIBLE);
                        }else{
                            toStation = stations.get(i);
                            toText.setText(toStation.getStation_description());
                            stageThree.setVisibility(View.VISIBLE);
                        }

                    }
                }).setPositiveButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();

        dialog.show();
    }

}
