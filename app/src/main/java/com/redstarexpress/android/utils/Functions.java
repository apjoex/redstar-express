package com.redstarexpress.android.utils;

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by sundayakinsete on 08/03/2018.
 */

public class Functions {

    public static void hideSoftKeyboard(Context activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                ((Activity)activity).getCurrentFocus().getWindowToken(), 0);
    }
}
