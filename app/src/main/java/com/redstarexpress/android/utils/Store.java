package com.redstarexpress.android.utils;

/**
 * Created by apjoex on 31/01/2018.
 */

public class Store {

    public static String locations = "[\n" +
            " {\n" +
            "   \"location\": \"Nsukka\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"26, University Road,\",\n" +
            "   \"City\": \"Nsukka\",\n" +
            "   \"State\": \"Nsukka\",\n" +
            "   \"Latitude\": 6.864455,\n" +
            "   \"Longitude\": 7.408288\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ogoja cross river\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"No 1, Lagos Street, Igoli, Ogoja cross river state\",\n" +
            "   \"City\": \"Cross river\",\n" +
            "   \"State\": \"cross-river\",\n" +
            "   \"Latitude\": 6.660049,\n" +
            "   \"Longitude\": 8.799726\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ogoja Abakaliki\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"8b Ogoja Road, Abakaliki\",\n" +
            "   \"City\": \"Abakaliki\",\n" +
            "   \"State\": \"Abakaliki\",\n" +
            "   \"Latitude\": 6.315772,\n" +
            "   \"Longitude\": 8.126083\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Awka\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"208, Ziks Avenue, Beside Diamond Bank Plc Opp NIPOST Awka\",\n" +
            "   \"City\": \"Awka\",\n" +
            "   \"State\": \"Anambra\",\n" +
            "   \"Latitude\": 6.212672,\n" +
            "   \"Longitude\": 7.071835\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Owerri\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"72, Wetheral Road Owerri\",\n" +
            "   \"City\": \"Owerri\",\n" +
            "   \"State\": \"Imo\",\n" +
            "   \"Latitude\": 5.487453,\n" +
            "   \"Longitude\": 7.038949\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Onitsha\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"47, New Market Road By Iboku Junction Onitsha\",\n" +
            "   \"City\": \"Onitsha\",\n" +
            "   \"State\": \"Onitsha\",\n" +
            "   \"Latitude\": 6.151735,\n" +
            "   \"Longitude\": 6.783492\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Enugu\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"77, Ogui Road, Enugu\",\n" +
            "   \"City\": \"Enugu\",\n" +
            "   \"State\": \"Enugu\",\n" +
            "   \"Latitude\": 6.445787,\n" +
            "   \"Longitude\": 7.49831\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Asaba\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"Asb 405,Nnebisi Road Asaba\",\n" +
            "   \"City\": \"Asaba\",\n" +
            "   \"State\": \"Delta\",\n" +
            "   \"Latitude\": 6.205421,\n" +
            "   \"Longitude\": 6.721872\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Okpe Road, Sapele\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"54 Okpe Road, Sapele\",\n" +
            "   \"City\": \"Sapele\",\n" +
            "   \"State\": \"Delta\",\n" +
            "   \"Latitude\": 5.887143,\n" +
            "   \"Longitude\": 5.685953\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ekpan, Delta State\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"Edmart Technologies Km 7, Refinery Road off Refinery Shaguolo Ekpan, Delta State\",\n" +
            "   \"City\": \"Delta\",\n" +
            "   \"State\": \"Delta\",\n" +
            "   \"Latitude\": 5.569137,\n" +
            "   \"Longitude\": 5.729848\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Warri Sapele Road\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"Dol Jul Venture 39 Warri Sapele Road, Warri\",\n" +
            "   \"City\": \"Sapele\",\n" +
            "   \"State\": \"warri\",\n" +
            "   \"Latitude\": 5.514005,\n" +
            "   \"Longitude\": 5.744623\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Effurun Sapele Rd\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"Rolad Claud 41 Effurun Sapele Rd, Warri\",\n" +
            "   \"City\": \"Sapele\",\n" +
            "   \"State\": \"warri\",\n" +
            "   \"Latitude\": 5.561064,\n" +
            "   \"Longitude\": 5.785847\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Warri\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"100, Effurun -Warri Road By Fidelty Bank Warri\",\n" +
            "   \"City\": \"Warri\",\n" +
            "   \"State\": \"Delta\",\n" +
            "   \"Latitude\": 5.573499,\n" +
            "   \"Longitude\": 5.793405\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Agbor\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"Ogboi Ventures Lagos /Asaba Road Near Old Union Bank After Bridge\",\n" +
            "   \"City\": \"Agbor\",\n" +
            "   \"State\": \"Benin\",\n" +
            "   \"Latitude\": 6.253671,\n" +
            "   \"Longitude\": 6.203737\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ekpoma\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"Market Square Ekpoma By Union Bank Ekpoma\",\n" +
            "   \"City\": \"Ekpoma\",\n" +
            "   \"State\": \"Benin\",\n" +
            "   \"Latitude\": 6.755001,\n" +
            "   \"Longitude\": 6.052197\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Auch\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"Auchi Otaru Road Opp Forte Oil Auchi\",\n" +
            "   \"City\": \"Auchi\",\n" +
            "   \"State\": \"Benin\",\n" +
            "   \"Latitude\": 7.07619,\n" +
            "   \"Longitude\": 6.268934\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Uselu-Lagos Road Benin\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"No 158 Uselu-Lagos Road Near Wesco Lotto Building Benin\",\n" +
            "   \"City\": \"Benin\",\n" +
            "   \"State\": \"Benin\",\n" +
            "   \"Latitude\": 6.360265,\n" +
            "   \"Longitude\": 5.620182\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Delta Crescent Benin City\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"No 5B Delta Crescent , By Kiko Hotel Off Ikpokpan Road Gra Benin City\",\n" +
            "   \"City\": \"Benin\",\n" +
            "   \"State\": \"Benin\",\n" +
            "   \"Latitude\": 6.307851,\n" +
            "   \"Longitude\": 5.625976\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Airport Rd Benin City\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"35, Airport Rd St. Paul's Catholic Church Benin City\",\n" +
            "   \"City\": \"Benin\",\n" +
            "   \"State\": \"Benin\",\n" +
            "   \"Latitude\": 6.313464,\n" +
            "   \"Longitude\": 5.599864\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Eket\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"58, Grace Bill Road Eket\",\n" +
            "   \"City\": \"Eket\",\n" +
            "   \"State\": \"Akwa ibom\",\n" +
            "   \"Latitude\": 4.636035,\n" +
            "   \"Longitude\": 7.931146\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Calabar\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"53, Ndidem Usang Iso Road Calabar\",\n" +
            "   \"City\": \"Calabar\",\n" +
            "   \"State\": \"Calabar\",\n" +
            "   \"Latitude\": 4.970729,\n" +
            "   \"Longitude\": 8.339169\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Aba Road,\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"42, Aba Road, Ikot Ekpene Obiageri Nwaogunna\",\n" +
            "   \"City\": \"uyo\",\n" +
            "   \"State\": \"Akwa ibom\",\n" +
            "   \"Latitude\": 5.180983,\n" +
            "   \"Longitude\": 7.694159\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Oron\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"113, Oron Road Uyo\",\n" +
            "   \"City\": \"Oron\",\n" +
            "   \"State\": \"Akwa ibom\",\n" +
            "   \"Latitude\": 5.016372,\n" +
            "   \"Longitude\": 7.938837\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Aba\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"8, Factory Road Aba\",\n" +
            "   \"City\": \"aba\",\n" +
            "   \"State\": \"abia\",\n" +
            "   \"Latitude\": 5.119242,\n" +
            "   \"Longitude\": 7.369215\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Hospital Road, bonny\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"49, Hospital Road, Bonny Island Rivers State\",\n" +
            "   \"City\": \"Bonny\",\n" +
            "   \"State\": \"bonny\",\n" +
            "   \"Latitude\": 4.436015,\n" +
            "   \"Longitude\": 7.17349\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Federal Secretariat Aba Road\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"Federal Secretariat Aba Road (Opposite Presidential Hotel) Port-Harcourt\",\n" +
            "   \"City\": \"Port Harcourt\",\n" +
            "   \"State\": \"Rivers\",\n" +
            "   \"Latitude\": 4.830592,\n" +
            "   \"Longitude\": 7.007255\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Elelenwo Road\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"Handy Place 33, Elelenwo Road GRA Phase 2, Port-Harcourt\",\n" +
            "   \"City\": \"Port Harcourt\",\n" +
            "   \"State\": \"Rivers\",\n" +
            "   \"Latitude\": 4.837018,\n" +
            "   \"Longitude\": 7.070368\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"East West Road Rumuodara\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"Jogrey Ventures Ltd Km 5, East West Road Rumuodara, Port Harcourt\",\n" +
            "   \"City\": \"Port Harcourt\",\n" +
            "   \"State\": \"Rivers\",\n" +
            "   \"Latitude\": 4.862211,\n" +
            "   \"Longitude\": 7.038524\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Onne Market\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"Onne Market B/4 Mr Biggs, Along Onne Road\",\n" +
            "   \"City\": \"Port Harcourt\",\n" +
            "   \"State\": \"Rivers\",\n" +
            "   \"Latitude\": 30.521135,\n" +
            "   \"Longitude\": -91.039019\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Aggrey Road\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"95, Aggrey Road Port Hacourt\",\n" +
            "   \"City\": \"Port Harcourt\",\n" +
            "   \"State\": \"Rivers\",\n" +
            "   \"Latitude\": 4.761768,\n" +
            "   \"Longitude\": 7.023428\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Aba Road Port Harcourt\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"LAAS Group House , 206 Aba Road\",\n" +
            "   \"City\": \"Port Harcourt\",\n" +
            "   \"State\": \"Rivers\",\n" +
            "   \"Latitude\": 4.833311,\n" +
            "   \"Longitude\": 7.007263\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ikwerre Road, Port- Harcourt\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"47, Ikwerre Road, Mile One, Port- Harcourt\",\n" +
            "   \"City\": \"Port Harcourt\",\n" +
            "   \"State\": \"Rivers\",\n" +
            "   \"Latitude\": 4.79059,\n" +
            "   \"Longitude\": 6.99978\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Oyigbo,\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"Integrated Business Solution Centre. 28, Location Road, Adjacent Zenith Bank, Oyigbo, River State.\",\n" +
            "   \"City\": \"Port Harcourt\",\n" +
            "   \"State\": \"Rivers\",\n" +
            "   \"Latitude\": 39.002578,\n" +
            "   \"Longitude\": -77.176914\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Azikiwe Road\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"El-Shaddai Business Centre 3 Azikiwe Road (Unity Bank Building) , Port –Harcourt\",\n" +
            "   \"City\": \"Port Harcourt\",\n" +
            "   \"State\": \"Port Harcourt\",\n" +
            "   \"Latitude\": 4.774679,\n" +
            "   \"Longitude\": 7.012767\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ohaeto Street,\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"52 Shipeolu Street Palmgrove, , CKDigital,\",\n" +
            "   \"City\": \"Port Harcourt\",\n" +
            "   \"State\": \"Rivers\",\n" +
            "   \"Latitude\": 4.799671,\n" +
            "   \"Longitude\": 7.005064\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"54D, Trans Amadi, Nkpogu, Port -Harcourt\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"54D, Trans Amadi, Nkpogu, Port -Harcourt\",\n" +
            "   \"City\": \"Port Harcourt\",\n" +
            "   \"State\": \"Port Harcourt\",\n" +
            "   \"Latitude\": 4.812571,\n" +
            "   \"Longitude\": 7.044039\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"169 Aba road waterlines , Port – Harcourt\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"169 Aba road waterlines , Port – Harcourt\",\n" +
            "   \"City\": \"Port Harcourt\",\n" +
            "   \"State\": \"Port Harcourt\",\n" +
            "   \"Latitude\": 4.817565,\n" +
            "   \"Longitude\": 7.00868\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Plot 192, Stadium Road , Port-Hacourt169, Aba Road Waterlines , Port Harcourt\",\n" +
            "   \"Regions\": \"EASTERN REGION\",\n" +
            "   \"Address\": \"Plot 192, Stadium Road , Port-Hacourt169, Aba Road Waterlines , Port Harcourt\",\n" +
            "   \"City\": \"Port Harcourt\",\n" +
            "   \"State\": \"Port Harcourt\",\n" +
            "   \"Latitude\": 4.829015,\n" +
            "   \"Longitude\": 7.015218\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Suleja\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"Suite 087 Park and Garden Complex Opp. General Hospital, Suleja.\",\n" +
            "   \"City\": \"Suleja\",\n" +
            "   \"State\": \"Suleja\",\n" +
            "   \"Latitude\": 9.191551,\n" +
            "   \"Longitude\": 7.178778\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Minna\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"Block E, Shop 4, Awwal Ibrahim Shopping Complex Opp. Abdulsalam Garage Farm Centre Tunga Minna\",\n" +
            "   \"City\": \"Minna\",\n" +
            "   \"State\": \"Minna\",\n" +
            "   \"Latitude\": 9.589072,\n" +
            "   \"Longitude\": 6.564848\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"No7 Kiyawa Road off Riyawa Road, Off Sani Abacha Way Jigawa State.\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"No7 Kiyawa Road off Riyawa Road, Off Sani Abacha Way Jigawa State.\",\n" +
            "   \"City\": \"Dutse\",\n" +
            "   \"State\": \"Jigawa\",\n" +
            "   \"Latitude\": 11.929119,\n" +
            "   \"Longitude\": 9.623097\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"No 1 Go-Slow Road By Ahmadu Bello Way, Birnin Kebbi, Kebbi State\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"No 1 Go-Slow Road By Ahmadu Bello Way, Birnin Kebbi, Kebbi State\",\n" +
            "   \"City\": \"Kebbi\",\n" +
            "   \"State\": \"Kebbi\",\n" +
            "   \"Latitude\": 12.452594,\n" +
            "   \"Longitude\": 4.199749\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"A.B.U Campus Express Centre @ Guidiance And Counselling Unit,Sammaru,Zaria.\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"A.B.U Campus Express Centre @ Guidiance And Counselling Unit,Sammaru,Zaria.\",\n" +
            "   \"City\": \"Zaria\",\n" +
            "   \"State\": \"Zaria\",\n" +
            "   \"Latitude\": 11.083313,\n" +
            "   \"Longitude\": 7.724697\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"No.22 Park Road Off Wharf Road Sabon Gari Zaria\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"No.22 Park Road Off Wharf Road Sabon Gari Zaria\",\n" +
            "   \"City\": \"Gari\",\n" +
            "   \"State\": \"Zaria\",\n" +
            "   \"Latitude\": 10.581827,\n" +
            "   \"Longitude\": 7.445548\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Zamfara\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"12, Zaria/Sokoto Road Gusau Beside Friday Tundu Wade Gusau Mosque Tundu Wade Zamfara State\",\n" +
            "   \"City\": \"Gusau\",\n" +
            "   \"State\": \"Zamfara\",\n" +
            "   \"Latitude\": 12.149355,\n" +
            "   \"Longitude\": 6.713264\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"88, Emir Yahaya Road, Sokoto\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"88, Emir Yahaya Road, Sokoto\",\n" +
            "   \"City\": \"sokoto\",\n" +
            "   \"State\": \"sokoto\",\n" +
            "   \"Latitude\": 13.052126,\n" +
            "   \"Longitude\": 5.236816\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Gombe\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"77, Jekada Fari Hospital Road, Gombe\",\n" +
            "   \"City\": \"Gombe\",\n" +
            "   \"State\": \"Gombe\",\n" +
            "   \"Latitude\": 10.288007,\n" +
            "   \"Longitude\": 11.162758\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Bauchi\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"11, Yandoka Road Bauchi State\",\n" +
            "   \"City\": \"Bauchi\",\n" +
            "   \"State\": \"Bauchi\",\n" +
            "   \"Latitude\": 10.319373,\n" +
            "   \"Longitude\": 9.827921\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"11, Kashim Ibrahim Way, By West End Maiduguri\",\n" +
            "   \"City\": \"Maiduguri\",\n" +
            "   \"State\": \"Maiduguri\",\n" +
            "   \"Latitude\": 11.834545,\n" +
            "   \"Longitude\": 13.10902\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Damaturu\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"Maiduguri Road Opp. Governor's Office Damaturu\",\n" +
            "   \"City\": \"Damaturu\",\n" +
            "   \"State\": \"Damaturu\",\n" +
            "   \"Latitude\": 11.743438,\n" +
            "   \"Longitude\": 11.979266\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Katsina\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"7, Yahaya Madaki Kaita House Opp. Shekina Restaurant Katsina\",\n" +
            "   \"City\": \"Katsina\",\n" +
            "   \"State\": \"Katsina\",\n" +
            "   \"Latitude\": 12.982489,\n" +
            "   \"Longitude\": 7.614861\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Muritala Mohammed Way Kano\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"139, Muritala Mohammed Way Kano State\",\n" +
            "   \"City\": \"kano\",\n" +
            "   \"State\": \"kano\",\n" +
            "   \"Latitude\": 12.01767,\n" +
            "   \"Longitude\": 8.521794\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Yakubu Gowon Way, Kaduna\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"1, Yakubu Gowon Way, Kaduna\",\n" +
            "   \"City\": \"Kaduna\",\n" +
            "   \"State\": \"Kaduna\",\n" +
            "   \"Latitude\": 10.513198,\n" +
            "   \"Longitude\": 7.430818\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ahmadu Bello Way, Kaduna\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"14, Ahmadu Bello Way, Kaduna\",\n" +
            "   \"City\": \"Kaduna\",\n" +
            "   \"State\": \"Kaduna\",\n" +
            "   \"Latitude\": 10.513198,\n" +
            "   \"Longitude\": 7.430818\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"26, Ahmadu Bello Way, Kaduna\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"26, Ahmadu Bello Way, Kaduna\",\n" +
            "   \"City\": \"Kaduna\",\n" +
            "   \"State\": \"Kaduna\",\n" +
            "   \"Latitude\": 10.512939,\n" +
            "   \"Longitude\": 7.430879\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Kachia Road Kaduna\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"6, Kachia Road Kaduna\",\n" +
            "   \"City\": \"Kaduna\",\n" +
            "   \"State\": \"Kaduna\",\n" +
            "   \"Latitude\": 10.488864,\n" +
            "   \"Longitude\": 7.419073\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ali Akilu Road, Kaduna\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"2A, Ali Akilu Road, Kaduna.\",\n" +
            "   \"City\": \"Kaduna\",\n" +
            "   \"State\": \"Kaduna\",\n" +
            "   \"Latitude\": 10.547346,\n" +
            "   \"Longitude\": 7.440851\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Jalingo\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"15, Hammanruwa Way Beside First Bank Jalingo\",\n" +
            "   \"City\": \"Jalingo\",\n" +
            "   \"State\": \"Jalingo\",\n" +
            "   \"Latitude\": 8.9,\n" +
            "   \"Longitude\": 11.366667\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Yola\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"No 35 Galadima Aminu Way, Jimeta Yola, Adamawa State\",\n" +
            "   \"City\": \"Yola\",\n" +
            "   \"State\": \"Adamawa\",\n" +
            "   \"Latitude\": 9.269285,\n" +
            "   \"Longitude\": 12.440788\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"37,Rwang Pam Street Beside Modern Bokshop Jos Plateau State\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"37,Rwang Pam Street Beside Modern Bokshop Jos Plateau State\",\n" +
            "   \"City\": \"Jos\",\n" +
            "   \"State\": \"Plateau\",\n" +
            "   \"Latitude\": 9.919839,\n" +
            "   \"Longitude\": 8.888577\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Murtala Muhammed Way\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"29, Murtala Muhammed Way Beside Mainstreet Bank\",\n" +
            "   \"City\": \"Jos\",\n" +
            "   \"State\": \"Jos\",\n" +
            "   \"Latitude\": 6.508351,\n" +
            "   \"Longitude\": 3.3722\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Lafia\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"Shop 28, Kaura Plaza Opp. PDP Secretariat Jos Rd., Lafia\",\n" +
            "   \"City\": \"Lafia\",\n" +
            "   \"State\": \"Jos\",\n" +
            "   \"Latitude\": 11.988266,\n" +
            "   \"Longitude\": 8.55834\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Lokoja\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"No 16, IBB Way, Beside UBA, Lokoja\",\n" +
            "   \"City\": \"Lokoja\",\n" +
            "   \"State\": \"Lokoja\",\n" +
            "   \"Latitude\": 7.79552,\n" +
            "   \"Longitude\": 6.73678\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Makurdi\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"12, Oturkpo Road, Makurdi\",\n" +
            "   \"City\": \"Makurdi\",\n" +
            "   \"State\": \"Makurdi\",\n" +
            "   \"Latitude\": 7.706548,\n" +
            "   \"Longitude\": 8.533431\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Maitama, Abuja\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"No. 38 Gana Street, Maitama, Abuja\",\n" +
            "   \"City\": \"Abuja\",\n" +
            "   \"State\": \"Abuja\",\n" +
            "   \"Latitude\": 9.078593,\n" +
            "   \"Longitude\": 7.499995\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Aminu Kano Crescent, Wuse 2, Abuja\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"Suite AFF 10, K-City Plaza, Aminu Kano Crescent, Wuse 2, Abuja\",\n" +
            "   \"City\": \"Abuja\",\n" +
            "   \"State\": \"Abuja\",\n" +
            "   \"Latitude\": 9.074014,\n" +
            "   \"Longitude\": 7.470328\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Wuse 2, Abuja\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"No. 59 Ademola Adetokunbo Crescent, Opp. Amigo Supermarket, Beside Fidelity Bank, Wuse 2, Abuja\",\n" +
            "   \"City\": \"Abuja\",\n" +
            "   \"State\": \"Abuja\",\n" +
            "   \"Latitude\": 9.07116,\n" +
            "   \"Longitude\": 7.483877\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Plot 735, Namadi Sambo Road, Idu Industrial Estate Idu FCT Abuja.\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"Plot 822 Tafawa Balewa Way, Opp. Nicon Luxury Hotel, CBD, Abuja\",\n" +
            "   \"City\": \"Abuja\",\n" +
            "   \"State\": \"Abuja\",\n" +
            "   \"Latitude\": 9.04542,\n" +
            "   \"Longitude\": 7.494028\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Tafawa Balewa Way, Abuja\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"Plot 882 Tafawa Balewa Way, Opp. Nicon Luxury Hotel, CBD, Abuja\",\n" +
            "   \"City\": \"Abuja\",\n" +
            "   \"State\": \"Abuja\",\n" +
            "   \"Latitude\": 9.04542,\n" +
            "   \"Longitude\": 7.494028\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Wuse Zone 3, Abuja\",\n" +
            "   \"Regions\": \"NORTHERN REGION\",\n" +
            "   \"Address\": \"No 3, Conakry Street, Wuse Zone 3, close to Nigeria Custom's Head Office, Abuja,\",\n" +
            "   \"City\": \"Abuja\",\n" +
            "   \"State\": \"Abuja\",\n" +
            "   \"Latitude\": 9.063325,\n" +
            "   \"Longitude\": 7.469716\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ogbomosho\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Ogbomosho Opp. Nigerian Baptist Theological Seminary,Randa Junction, Ilorin Road Ogbomosho\",\n" +
            "   \"City\": \"Ogbomosho\",\n" +
            "   \"State\": \"Oyo\",\n" +
            "   \"Latitude\": 8.130662,\n" +
            "   \"Longitude\": 4.232209\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ilesha\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"E5 Oke-Eso Street, Behind Akewusola Chemist, Ilesa\",\n" +
            "   \"City\": \"Ilesha\",\n" +
            "   \"State\": \"Osun\",\n" +
            "   \"Latitude\": 7.623882,\n" +
            "   \"Longitude\": 4.781499\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Oshogbo\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Oshogbo Suite B8, Treasure Plaza, 10 Obafemi Awolowo Way, Osogbo.\",\n" +
            "   \"City\": \"Oshogbo\",\n" +
            "   \"State\": \"Osun\",\n" +
            "   \"Latitude\": 7.781304,\n" +
            "   \"Longitude\": 4.556688\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ado- Ekiti\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"74,Ajilosun Street,Adjacent Oando Petrol Station.Ado Ekiti\",\n" +
            "   \"City\": \"Ado-ekiti\",\n" +
            "   \"State\": \"Ekiti\",\n" +
            "   \"Latitude\": 7.601533,\n" +
            "   \"Longitude\": 5.221045\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ore Agency\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Ore Agency No 29, Ondo/Okitipupa Road, (Adeyalo Junction) Ore\",\n" +
            "   \"City\": \"Ore\",\n" +
            "   \"State\": \"Ogun\",\n" +
            "   \"Latitude\": 6.504172,\n" +
            "   \"Longitude\": 4.771382\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ademulegun Road Ondo\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Ondo Agency No,258 Ademulegun Road Ondo State\",\n" +
            "   \"City\": \"Akure\",\n" +
            "   \"State\": \"Ondo\",\n" +
            "   \"Latitude\": 7.109168,\n" +
            "   \"Longitude\": 4.837412\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Akure\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Akure 49, Oba Adeside Road Opp. Toyin Bookshop Akure\",\n" +
            "   \"City\": \"Akure\",\n" +
            "   \"State\": \"Ondo\",\n" +
            "   \"Latitude\": 7.252304,\n" +
            "   \"Longitude\": 5.199192\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Sagamu\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Sagamu 127, Akarigbo Street, Opp Total Filling Station Ijokun Sagamu\",\n" +
            "   \"City\": \"Sagamu\",\n" +
            "   \"State\": \"Ogun\",\n" +
            "   \"Latitude\": 6.820833,\n" +
            "   \"Longitude\": 3.920833\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ijebu-Ode\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"65. Ijebu-Ode 12, Ibadan Rd Beside Old First Bank, Ijebu-Ode\",\n" +
            "   \"City\": \"Ijebu-ode\",\n" +
            "   \"State\": \"Osun\",\n" +
            "   \"Latitude\": 6.820833,\n" +
            "   \"Longitude\": 3.920833\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Beside Hotlink Sport Shop, OAU Ile-Ife Osun\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Beside Hotlink Sport Shop, OAU Ile-Ife Osun\",\n" +
            "   \"City\": \"Ile-Ife\",\n" +
            "   \"State\": \"Osun\",\n" +
            "   \"Latitude\": 7.516518,\n" +
            "   \"Longitude\": 4.528561\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ibadan Road Ile-Ife\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"No. 100 Ibadan Road Adjacent Apex Medical Centre, Ile-Ife Osun State\",\n" +
            "   \"City\": \"Ile-ife\",\n" +
            "   \"State\": \"Osun\",\n" +
            "   \"Latitude\": 7.493134,\n" +
            "   \"Longitude\": 4.511472\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"CRM Lagos Ibadan Expressway\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"CRM Beside Access Bank, Rccg Redemption Camp Lagos Ibadan Expressway\",\n" +
            "   \"City\": \"Lagos Ibadan Express way\",\n" +
            "   \"State\": \"Abeokuta\",\n" +
            "   \"Latitude\": 6.815337,\n" +
            "   \"Longitude\": 3.452812\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Alabata, Abeokuta\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"FUNAAB, Consult Room 228, Coplant Building, Unaab, Alabata, Abeokuta\",\n" +
            "   \"City\": \"Abeokuta\",\n" +
            "   \"State\": \"Abeokuta\",\n" +
            "   \"Latitude\": 7.223307,\n" +
            "   \"Longitude\": 3.44034\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Oke-Ilewo, Abeokuta\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Oke-Ilewo Centre 8 Lalubu Street, Oke-Ilewo, Abeokuta\",\n" +
            "   \"City\": \"Abeokuta\",\n" +
            "   \"State\": \"Abeokuta\",\n" +
            "   \"Latitude\": 7.135453,\n" +
            "   \"Longitude\": 3.339443\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ibara, Abeokuta\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"UACN Centre UACN Complex, Ibara, Abeokuta\",\n" +
            "   \"City\": \"Abeokuta\",\n" +
            "   \"State\": \"Abeokuta\",\n" +
            "   \"Latitude\": 7.135423,\n" +
            "   \"Longitude\": 3.337125\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Quarry Road, Abeokuta\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Agbeloba House, 56, Quarry Road, Abeokuta\",\n" +
            "   \"City\": \"Abeokuta\",\n" +
            "   \"State\": \"Abeokuta\",\n" +
            "   \"Latitude\": 7.135965,\n" +
            "   \"Longitude\": 3.322222\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Offa, Kwara\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"57. Offa No. 102, Olofa Way, Beside Old Nepa Office. Offa, Kwara State.\",\n" +
            "   \"City\": \"Offa\",\n" +
            "   \"State\": \"Kwara\",\n" +
            "   \"Latitude\": 8.143582,\n" +
            "   \"Longitude\": 4.701124\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ibrahim Taiwo Road, Kwara\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"56. 163 Ibrahim Taiwo Road, Opp. Eco Bank, Ilorin, Kwara State\",\n" +
            "   \"City\": \"Ilorin\",\n" +
            "   \"State\": \"Kwara\",\n" +
            "   \"Latitude\": 8.482314,\n" +
            "   \"Longitude\": 4.549636\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Mokola\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Premier Hotel ,Shop L143, Premier Hotel, Mokola, Ibadan\",\n" +
            "   \"City\": \"Ibadan\",\n" +
            "   \"State\": \"Oyo\",\n" +
            "   \"Latitude\": 7.408651,\n" +
            "   \"Longitude\": 3.894405\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Bodija Agency\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Bodija Agency Awolowo Junction, Beside Gtbank, Bodija\",\n" +
            "   \"City\": \"Ibadan\",\n" +
            "   \"State\": \"Oyo\",\n" +
            "   \"Latitude\": 7.417015,\n" +
            "   \"Longitude\": 3.910913\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Agbowo, Ibadan\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Agowo Shopping Complex, Opp. University Of Ibadan Main Gate, Agbowo, Ibadan\",\n" +
            "   \"City\": \"Ibadan\",\n" +
            "   \"State\": \"Oyo\",\n" +
            "   \"Latitude\": 7.441257,\n" +
            "   \"Longitude\": 3.906212\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Iwo Road\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Iwo Road Agency Abayomi Bus Stop, Opp Baloon Building, Iwo Road\",\n" +
            "   \"City\": \"Ibadan\",\n" +
            "   \"State\": \"Oyo\",\n" +
            "   \"Latitude\": 7.402984,\n" +
            "   \"Longitude\": 3.9399\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Dugbe, Ibadan\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Cocoa Mall, Shop H31, Heritage Mall, Dugbe, Ibadan.\",\n" +
            "   \"City\": \"Ibadan\",\n" +
            "   \"State\": \"Oyo\",\n" +
            "   \"Latitude\": 7.386738,\n" +
            "   \"Longitude\": 3.880008\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Adamasingba Ibadan\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Shop 153, Adamasingba Shopping Complex, Adekunle Fajuyi Road, Adamasingba Ibadan\",\n" +
            "   \"City\": \"Ibadan\",\n" +
            "   \"State\": \"Oyo\",\n" +
            "   \"Latitude\": 7.396206,\n" +
            "   \"Longitude\": 3.886059\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"UCH Ibadan\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"U. C. H. Office College of Medicine Bookshop, UCH Ibadan\",\n" +
            "   \"City\": \"Ibadan\",\n" +
            "   \"State\": \"Oyo\",\n" +
            "   \"Latitude\": 7.401113,\n" +
            "   \"Longitude\": 3.90406\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ring Road, Ibadan\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Fedex House, Sw8/1407 Ring Road, Ajeigbe Bus Stop, Ibadan\",\n" +
            "   \"City\": \"Ibadan\",\n" +
            "   \"State\": \"Oyo\",\n" +
            "   \"Latitude\": 7.35419,\n" +
            "   \"Longitude\": 3.87392\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Agric Isawo Road Ikorodu\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Plot 243, Platform plaza, opposite 2nd Mobil Petrol Station Agric Isawo Road Ikorodu\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.625693,\n" +
            "   \"Longitude\": 3.484227\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ojota\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Zatim Global Services Ltd Block 1, Suite 11, Odua Shopping Plaza Ojota, Lagos\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.58483,\n" +
            "   \"Longitude\": 3.37731\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ikorodu\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"250 Ikorodu Lagos Road, Ikorodu, Lagos\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.666393,\n" +
            "   \"Longitude\": 3.51602\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Akoka Yaba\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"4 St.Finbarrs College Road, Akoka Yaba Lagos.\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.524298,\n" +
            "   \"Longitude\": 3.385611\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Gbagada\",\n" +
            "   \"Regions\": \"No.6 Lanre Awolokun Str. Gbagada\",\n" +
            "   \"Address\": \"Lagos\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \" 6.547324\",\n" +
            "   \"Latitude\": 3.383202,\n" +
            "   \"Longitude\": null\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Lasu, Ojo\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Anupa-Lasu Cooperative Multipurpose Society Limited, Cooperative Arena, Lasu, Ojo.\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.463502,\n" +
            "   \"Longitude\": 3.200831\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ojuelegba\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"93 Ojuelegba Road\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.510104,\n" +
            "   \"Longitude\": 3.363537\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ojo Alaba\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Alaba 156 Olojo Drive,Alaba Int’l Mkt. Ojo Alaba\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.457558,\n" +
            "   \"Longitude\": 3.203986\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ikorodu Rd\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"216 Ikorodu Rd. (Olabode House)\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.578337,\n" +
            "   \"Longitude\": 3.53359\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Egbeda\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Alpha Tunad-Edu Consul 123/125 Idimurd. ,Egbeda\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.603246,\n" +
            "   \"Longitude\": 3.292944\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ojodu Berger\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"98, Ogunnusi Road, Ojodu Berger Beside Grammar School Busstop Lagos\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.639905,\n" +
            "   \"Longitude\": 3.357911\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Oregun, Ikeja\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"59 Kudirat Abiola Way Oregun, Ikeja\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \" 6.59894\",\n" +
            "   \"Latitude\": 3.366288,\n" +
            "   \"Longitude\": null\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"GRA Ikeja\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"40 Oduduwa Way GRA Ikeja\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.57241,\n" +
            "   \"Longitude\": 3.354869\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Fagba Agege\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Suite C3, Opp.Ist Bank Plc,Fagba Agege\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.627886,\n" +
            "   \"Longitude\": 3.331713\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ogba\",\n" +
            "   \"Regions\": \"Shop 272 Ogba Retail Market, Ogba\",\n" +
            "   \"Address\": \"Lagos\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \" 6.630394\",\n" +
            "   \"Latitude\": 3.343457,\n" +
            "   \"Longitude\": null\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Agbara\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Suite 51 Galaxy Shopping Complex Agbara\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.505162,\n" +
            "   \"Longitude\": 3.085421\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Festac\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"5th Ave.Opp, Nitel Festac\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.450106,\n" +
            "   \"Longitude\": 3.258953\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Maryland\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"No 4, comfort Adeniyi street adjacent to chardon Restaurent,mende Maryland.\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 41.582844,\n" +
            "   \"Longitude\": -81.209706\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Sabo Yaba\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"284 Herbert Macaulay Way,Sabo Yaba.\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.50367,\n" +
            "   \"Longitude\": 3.377641\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Abiola Ikeja\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"5A Simbiat Abiola Road. Ikeja\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.609431,\n" +
            "   \"Longitude\": 3.353721\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Isolo\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Mode4r, 2B Rasom Street, Isolo, Lagos\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.5004,\n" +
            "   \"Longitude\": 3.30657\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Opebi Ikeja\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"N0 74 Opebi Road, Ikeja\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.587079,\n" +
            "   \"Longitude\": 3.363008\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Akran Avenue, Ikeja\",\n" +
            "   \"Regions\": \"—No categories\",\n" +
            "   \"Address\": \"40 Oba Akran Avenue, Ikeja.\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.602772,\n" +
            "   \"Longitude\": 3.337876\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ilupeju\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"34/36 Association Avenue, Opp. Total Filling Station, Ilupeju\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.547,\n" +
            "   \"Longitude\": 3.36517\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ota\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Shop 28 Obanibashiri Complex Ota.\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 35.561257,\n" +
            "   \"Longitude\": 139.716051\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Apapa\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"No 1 Commercial Road, Apapa\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.442666,\n" +
            "   \"Longitude\": 3.373309\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Surulere\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"No 2 Adeniran Ogunsanya Street, Surulere\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.498098,\n" +
            "   \"Longitude\": 3.358226\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Toyin Sreet, Ikeja\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"50/52 Toyin Sreet, Ikeja\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.595516,\n" +
            "   \"Longitude\": 3.352523\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Allen Avenue, Ikeja\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"30 Allen Avenue, Ikeja\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.599256,\n" +
            "   \"Longitude\": 3.353513\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Mafoluku\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"40/42 Old Ewu Road. Mafoluku . Lagos\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.554015,\n" +
            "   \"Longitude\": 3.330749\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Igbosere agency\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"G.Ike 199, Igbosere Road, Ground Floor Rasaki Place Opp High Court Lagos Island\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.449466,\n" +
            "   \"Longitude\": 3.401536\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Geetee, Suite 107 Dolphin Estate, Ikoyi\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Geetee, Suite 107 Dolphin Estate, Ikoyi\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.455498,\n" +
            "   \"Longitude\": 3.411967\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ado Road Ajah Agency\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Husbra, 1st Floor Suite 7, Iyabus Plaza Ado Rd., Ajah\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.48393,\n" +
            "   \"Longitude\": 3.574105\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Lekki Phase 1 Agency\",\n" +
            "   \"Regions\": \"—No categories\",\n" +
            "   \"Address\": \"Defuntoks, Plot 300, Admiralty Way, Lekki Phase 1, Lekki Lagos\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.447793,\n" +
            "   \"Longitude\": 3.475976\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Ikota Shopping Complex Ajah, Agency\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Cynergy, Suite E149, Road 1, Opp Fidelity, Ikota Shopping Complex Ajah, Lagos\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.469289,\n" +
            "   \"Longitude\": 3.574415\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"South Pavillon Agency\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Clock Work, 2nd Floor South Pavillon, Beside National Assembly Annex TBS.\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 53.432884,\n" +
            "   \"Longitude\": -2.700648\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Tafawa Balewa Square Agency\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"10. Omazont, Suite 13, Tafawa Balewa Square Complex Lagos\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.449287,\n" +
            "   \"Longitude\": 3.404822\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Lekki Epe Express Way\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Suite A17 Cherub Mall, Beside Chevy View Estate/Oakwood Hotel Km 18 Lekki Epe Express Way, Ajah\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.440626,\n" +
            "   \"Longitude\": 3.526734\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Admiralty Lekki phase 1\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"11, Admiralty Way by KFC Lekki Phase 1\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.440125,\n" +
            "   \"Longitude\": 3.455514\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Belanova Victorial Island\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Belanova 55a Adeola Odeku Street, Victoria Island\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.430333,\n" +
            "   \"Longitude\": 3.4182\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Kakawa CMS\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"27, Kakawa Street Behind CMS Bookshop Lagos\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.452508,\n" +
            "   \"Longitude\": 3.390155\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Broad Street\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Broad Street Give and Take Shop, Broad Street Lagos\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.455065,\n" +
            "   \"Longitude\": 3.386298\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Keffi Awolowo\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"15 Keffi Street Off Awolowo Rd Ikoyi Lagos\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.44455,\n" +
            "   \"Longitude\": 3.41488\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Awolowo Ikoyi\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"138, Awolowo Road Ikoyi Lagos\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.444659,\n" +
            "   \"Longitude\": 3.425361\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Taylor Victoria Island\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"Taylor 5, Idowu Taylor Street Victoria Island Lagos\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.433099,\n" +
            "   \"Longitude\": 3.425627\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Lalupon Ikoyi\",\n" +
            "   \"Regions\": \"WESTERN REGION\",\n" +
            "   \"Address\": \"2B, Lalupon Close, Off Keffi , Ikoyi,\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.44365,\n" +
            "   \"Longitude\": 3.41486\n" +
            " },\n" +
            " {\n" +
            "   \"location\": \"Head Office\",\n" +
            "   \"Regions\": \"—No categories\",\n" +
            "   \"Address\": \"70, International Airport Road, Ikeja\",\n" +
            "   \"City\": \"Lagos\",\n" +
            "   \"State\": \"Lagos\",\n" +
            "   \"Latitude\": 6.596489,\n" +
            "   \"Longitude\": 3.303515\n" +
            " }\n" +
            "]";

}
